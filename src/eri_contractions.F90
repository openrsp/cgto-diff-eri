! Copyright 2012 Andreas J. Thorvaldsen
! This file is made available under the terms of the
! GNU Lesser General Public License version 3.

!> @file Contains module eri_contractions

!> Electron-repulsion-integral contractions over sets of integrals 
!> (AB|CD) for one CGTO quadruple
module eri_contractions

  use basis_set, only: cgto
  use matrix_lowlevel, only: openrsp_matrix, mat_remove, mat_mpi_bcast, matrix => openrsp_matrix

  implicit none

  public contract_with_density
  public ctr_arg
  public set_eri_contractions_xfac
#if defined(VAR_MPI)
  public eri_ctrs_mpi_bcast
  public eri_ctrs_mpi_reduce
#endif
  public eri_ctrs_destroy

  !> A 'contraction' is either of 'average' type, or of 'Fock' type,
  !> depending on whether pointer @param average is associated.
  !> A fock contraction adds two-electron contribution to *one*
  !> Fock matrix given in @param fock_or_dens, whose geometrical
  !> coordinate(s) is chosen by @param comp. @param ncor determines
  !> the dimensions of average (implicitly).
  type ctr_arg
     integer               :: geo, comp, ncor
     type(matrix), pointer :: dens, fock_or_dens
     real(8),      pointer :: average(:)
  end type

  private

! exchange factor, 1 for HF, 0 for non-hybrid DFT, between 0 and 1 for hybrids
  real(8), save :: xfac = 1.0d0
  
#if defined(VAR_MPI)
  ! used on worker process
  type(matrix), target, save, allocatable :: wrk_dens(:)
  type(matrix), target, save, allocatable :: wrk_fock(:)
  real(8), target, save, allocatable :: wrk_ave(:)
#endif

contains

  subroutine set_eri_contractions_xfac(f)
    real(8), intent(in) :: f
    xfac = f
  end subroutine


  subroutine contract_with_density(A, B, C, D, gab, gcd, ng, eri, arg)
    type(cgto), intent(in) :: A, B, C, D
    integer,    intent(in) :: gab, gcd, ng
    real(8),    intent(in) :: eri(ng, size(A%ctr,1)*(2*A%mom+1), &
                                      size(B%ctr,1)*(2*B%mom+1), &
                                      size(C%ctr,1)*(2*C%mom+1), &
                                      size(D%ctr,1)*(2*D%mom+1))
    type(ctr_arg), intent(inout) :: arg(:)
    real(8) ctr(ng)
    integer iarg, i, j, k, l

    do iarg = 1, size(arg)
       ! average (any order)
       if (associated(arg(iarg)%average)) then
          call coul_exch_ave(arg(iarg)%dens%nrow, &
                             arg(iarg)%dens%elms, &
                             arg(iarg)%fock_or_dens%elms)
          call accumulate_ave(arg(iarg)%ncor, arg(iarg)%average)
       ! unperturbed Fock matrix
       else if (gab == 0 .and. gcd == 0) then
          call coul_exch_mat(1, arg(iarg)%dens%nrow, &
                                arg(iarg)%dens%elms, &
                                arg(iarg)%fock_or_dens%elms)
       else ! ... or Fock matrix (any order)
          call fock_combinations(arg(iarg)%ncor, &
                                 arg(iarg)%dens, &
                                 arg(iarg)%comp, &
                                 (/A%icent,B%icent,C%icent,D%icent/), &
                                 arg(iarg)%fock_or_dens)
       end if
    end do

  contains


    subroutine coul_exch_ave(nbas, dens1, dens2)
      integer, intent(in) :: nbas
      real(8), intent(in) :: dens1(nbas, nbas, *)
      real(8), intent(in) :: dens2(nbas, nbas, *)
      integer i, j, k, l, ii, jj, kk, ll
      real(8) :: f
      ! contract with pair of density matrices
      ctr = 0
      l = D%ibas + 1
      do ll=1, size(eri,5)
         k = C%ibas + 1
         do kk=1, size(eri,4)
            j = B%ibas + 1
            do jj=1, size(eri,3)
               i = A%ibas + 1
               do ii=1, size(eri,2)

                  f =    2.0d0*dens1(i, j, 1)*dens2(k, l, 1)
                  f = f - xfac*dens1(i, l, 1)*dens2(k, j, 1)

#ifdef PRG_DIRAC
                  f = f + xfac*dens1(i, l, 2)*dens2(k, j, 2)
                  f = f + xfac*dens1(i, l, 3)*dens2(k, j, 3)
                  f = f + xfac*dens1(i, l, 4)*dens2(k, j, 4)
#endif

                  ctr(:) = ctr(:) + f*eri(:, ii, jj, kk, ll)

                  i = i + 2*A%mom + 1
                  if (i > A%ibas + size(eri,2)) i = i - size(eri,2) + 1
               end do
               j = j + 2*B%mom + 1
               if (j > B%ibas + size(eri,3)) j = j - size(eri,3) + 1
            end do
            k = k + 2*C%mom + 1
            if (k > C%ibas + size(eri,4)) k = k - size(eri,4) + 1
         end do
         l = l + 2*D%mom + 1
         if (l > D%ibas + size(eri,5)) l = l - size(eri,5) + 1
      end do
    end subroutine



    subroutine accumulate_ave(ncor, ave)
      integer, intent(in)    :: ncor
      real(8), intent(inout) :: ave(ncor**(gab+gcd))
      real(8) c33(3**(gab+gcd),2) !scratch for unpacking of indices to 3^m
      integer tab, iab, icd, na, nb, nc, nd, ga, gb, gc, gd, i2, i
      integer divab, divcd, offset
      ! the total first electron dimension of ctr
      tab = (gab+1)*(gab+2)*(gab+3)*(gab+4)*(gab+5)/120
      icd = 0
      do gd = 0, gcd
         gc = gcd-gd
         iab = 0
         ! factorial divisor for C and D
         divcd = product((/(i,i=1,gc)/)) * product((/(i,i=1,gd)/))
         do gb = 0, gab
            ga = gab-gb
            na = (ga+1)*(ga+2)/2
            nb = (gb+1)*(gb+2)/2
            nc = (gc+1)*(gc+2)/2
            nd = (gd+1)*(gd+2)/2
            ! factorial divisor for A and B
            divab = product((/(i,i=1,ga)/)) * product((/(i,i=1,gb)/))
            ! determine which c33(:,i2) to start in, in order to
            ! end up in c33(:,1)
            i2 = 1 + mod(count((/ga>1,gb>1,gc>1,gd>1/)), 2)
            ! copy-reshape-scale from ctr to c33
            call copy_scale_rows(iab, na*nb, tab, nc*nd, &
                                 divab * divcd,          &
                                 ctr(tab*icd+1 : tab*(icd+nc*nd)), &
                                 c33(:na*nb*nc*nd, i2))
            iab = iab + na*nb
            ! unpack indices of A, if needed
            if (ga > 1) then
               call unpack_multicart(1, ga, nb*nc*nd, &
                          c33(:   na*nb*nc*nd,   i2), &
                          c33(:3**ga*nb*nc*nd, 3-i2))
               na = 3**ga
               i2 = 3-i2
            end if
            ! unpack indices of B, if needed
            if (gb > 1) then
               call unpack_multicart(na, gb, nc*nd, &
                        c33(:na*   nb*nc*nd,   i2), &
                        c33(:na*3**gb*nc*nd, 3-i2))
               nb = 3**gb
               i2 = 3-i2
            end if
            ! unpack indices of C, if needed
            if (gc > 1) then
               call unpack_multicart(na*nb, gc, nd, &
                        c33(:na*nb*   nc*nd,   i2), &
                        c33(:na*nb*3**gc*nd, 3-i2))
               nc = 3**gc
               i2 = 3-i2
            end if
            ! unpack indices of D, if needed
            if (gd > 1) then
               call unpack_multicart(na*nb*nc, gd, 1, &
                                c33(:na*nb*nc*nd, 2), &
                                c33(:           , 1))
               nd = 3**gd
            end if
            ! compute offset into average
            offset = 0
            do i = 1, gd
               offset = 3*(D%icent-1) + ncor * offset
            end do
            do i = 1, gc
               offset = 3*(C%icent-1) + ncor * offset
            end do
            do i = 1, gb
               offset = 3*(B%icent-1) + ncor * offset
            end do
            do i = 1, ga
               offset = 3*(A%icent-1) + ncor * offset
            end do
            ! accumulate unpacked derivatives into ave
            call add_unpacked_to_ave(gab+gcd, c33(:,1), ncor, &
                                     offset, ave)
         end do
         ! increment offset
         icd = icd + (gc+1)*(gc+2)/2 * (gd+1)*(gd+2)/2
      end do
    end subroutine



    subroutine coul_exch_mat(comp, nbas, dens, fock)
      integer, intent(in)    :: comp, nbas
      real(8), intent(in)    :: dens(nbas, nbas, *)
      real(8), intent(inout) :: fock(nbas, nbas, *)
      integer i, j, k, l, ii, jj, kk, ll
      real(8) :: f
      l = D%ibas + 1
      do ll=1, size(eri,5)
         k = C%ibas + 1
         do kk=1, size(eri,4)
            j = B%ibas + 1
            do jj=1, size(eri,3)
               i = A%ibas + 1
               do ii=1, size(eri,2)

                  f = eri(comp,ii,jj,kk,ll)

                  fock(i, j, 1) = fock(i, j, 1) + 2.0d0*dens(k, l, 1)*f ! coulomb
                  fock(i, l, 1) = fock(i, l, 1) -  xfac*dens(j, k, 1)*f ! exchange
#ifdef PRG_DIRAC
                  fock(i, l, 2) = fock(i, l, 2) +  xfac*dens(j, k, 2)*f
                  fock(i, l, 3) = fock(i, l, 3) +  xfac*dens(j, k, 3)*f
                  fock(i, l, 4) = fock(i, l, 4) +  xfac*dens(j, k, 4)*f
#endif

                  i = i + 2*A%mom + 1
                  if (i > A%ibas + size(eri,2)) i = i - size(eri,2) + 1
               end do
               j = j + 2*B%mom + 1
               if (j > B%ibas + size(eri,3)) j = j - size(eri,3) + 1
            end do
            k = k + 2*C%mom + 1
            if (k > C%ibas + size(eri,4)) k = k - size(eri,4) + 1
         end do
         l = l + 2*D%mom + 1
         if (l > D%ibas + size(eri,5)) l = l - size(eri,5) + 1
      end do
    end subroutine


    subroutine fock_combinations(ncor, dens, comp, cent, fock)
      integer,      intent(in)    :: ncor, comp, cent(4)
      type(matrix), intent(in)    :: dens
      type(matrix), intent(inout) :: fock
      integer ga, gb, gc, gd, iab, icd, nab, na, nc, ideriv, prefac
      integer t(4), x(4), y(4), z(4), xyz(4)
      ! first 'decode' the requested component 'comp'
      if (.not.decode_and_count_comp(ncor, gab+gcd, comp, &
                                     cent, x, y, z)) return
      ! first calculate leading dim of derivatives
      nab = (gab+1)*(gab+2)*(gab+3)*(gab+4)*(gab+5)/120
      ! loop over blocks CC..C CC..D ... DD..D of derivs for el-2
      icd = 0
      do gd = 0, gcd
         gc = gcd - gd
         nc = (gc+1)*(gc+2)/2
         ! loop over blocks AA..A AA..B ... BB..B of derivs for el-1
         iab = 0
         do gb = 0, gab
            ga = gab - gb
            na = (ga+1)*(ga+2)/2
            ! reset/init multiplicities accoding to ga gb gc gd.
            ! If it doesn't fit (reset_mult = F), skip
            if (reset_mult(cent, (/ga,gb,gc,gd/), x, y, z, prefac)) then
               ! loop over all combinations of indices contributing
               do
                  ! calculate the index from the multiplicities
                  xyz(:) = y + z*(2*x+2*y+z+3)/2
                  ideriv = 1 + iab + xyz(1) + na*xyz(2) &
                      + nab * (icd + xyz(3) + nc*xyz(4))
                  ! perform contraction
                  do i = 1, prefac
                     call coul_exch_mat(ideriv, dens%nrow, dens%elms, &
                                                           fock%elms)
                  end do
                  ! increment
                  if (.not.increment_mult(cent, x, y, z, prefac)) exit
               end do
            end if
            iab = iab + na * (gb+1)*(gb+2)/2
         end do
         icd = icd + nc * (gd+1)*(gd+2)/2
      end do
    end subroutine

  end subroutine



  !> simple inline to copy some rows from matrix art over to arn,
  !> while dividing by integer div
  subroutine copy_scale_rows(i, n, t, n2, div, art, arn)
    integer, intent(in)    :: i, n, t, n2, div
    real(8), intent(in)    :: art(t,n2)
    real(8), intent(inout) :: arn(n,n2)
    arn(:,:) = 1d0/div * art(i+1:i+n,:)
  end subroutine


  !> unpack/unravel/stretch triangular indices for x^mx*y^my*z^mz (mx+my+mz=@param m)
  !> to full 3**@param m tensor. @param l and @param t are leading and trailing dimensions.
  subroutine unpack_multicart(l, m, t, p, r)
    integer, intent(in)  :: l, m, t !leading, momentum, trailing
    real(8), intent(in)  :: p(l,(m+1)*(m+2)/2,t)
    real(8), intent(out) :: r(l,3**m,t)
    integer ii, iy, iz, mm, my, mz, s
    iy=0; iz=0; mm=1; my=0; mz=0
    do ii = 1, 3**m
       r(:,ii,:) = p(:,mm,:)
       do s=0, m-1
          ! x -> y transition
          if (.not.btest(iy,s) .and. .not.btest(iz,s)) then
             iy = ibset(iy,s)
             mm = mm+1
             my = my+1
             exit
          ! y -> z transition
          else if (btest(iy,s)) then
             iy = ibclr(iy,s)
             iz = ibset(iz,s)
             mm = mm + (m-mz)
             my = my-1
             mz = mz+1
             exit
          ! z -> x transition (followed by 'carry')
          else
             iz = ibclr(iz,s)
             mm = mm - (m-mz) - 2
             mz = mz-1
          end if
       end do
    end do
  end subroutine


  !> add ctr(3,3,...) to ave(nc,nc,...) at effective offset off
  subroutine add_unpacked_to_ave(geo, ctr, nc, off, ave)
    integer, intent(in)    :: geo, nc, off
    real(8), intent(in)    :: ctr(3**geo)
    real(8), intent(inout) :: ave(nc**geo)
    integer i, y, z, j, s
    i=off+1; y=0; z=0; 
    do j = 1, 3**geo
       ave(i) = ave(i) + ctr(j)
       ! increment i
       do s = 0, geo-1
          ! x -> y transition
          if (.not.btest(y,s) .and. .not.btest(z,s)) then
             y = ibset(y,s)
             i = i + nc**s
             exit
          ! y -> z transition
          else if (btest(y,s)) then
             y = ibclr(y,s)
             z = ibset(z,s)
             i = i + nc**s
             exit
          ! z -> x transition (followed by 'carry')
          else
             z = ibclr(z,s)
             i = i - 2*nc**s
          end if
       end do
    end do
  end subroutine


  function decode_and_count_comp(ncor, deriv, comp, cent, x, y, z)
    integer, intent(in)  :: ncor     !number of coords
    integer, intent(in)  :: deriv    !order of derivative
    integer, intent(in)  :: comp     !total component index
    integer, intent(in)  :: cent(4)  !center numbers for A B C D
    integer, intent(out) :: x(4), y(4), z(4) !multiplicities of x y z of center A B C D
    logical decode_and_count_comp
    integer idx, xyz, cen, i, j, t(4)
    decode_and_count_comp = .false.
    x=0; y=0; z=0
    idx = comp - 1
    do i = 1, deriv
       cen = mod(idx, ncor)
       idx = idx/ncor
       xyz = 1 + mod(cen, 3)
       cen = 1 + cen/3
       do j = 1, 4
          if (cent(j) /= cen) cycle
          if (xyz==1) x(j) = x(j)+1
          if (xyz==2) y(j) = y(j)+1
          if (xyz==3) z(j) = z(j)+1
          exit
       end do
       ! if 'cen' not among A B C D's icent, these derivatives won't
       if (j==5) return !contribute to component 'comp', so return false
    end do
    decode_and_count_comp = .true.
  end function


  function increment_mult(c, x, y, z, f)
    integer, intent(in)    :: c(4)           !A B C D%icent
    integer, intent(inout) :: x(4),y(4),z(4) !ord of x- y- z-deriv
    integer, intent(inout) :: f              !multinomial prefactor
    logical increment_mult
    integer i, j, t(4)
    increment_mult = .false.
    t(:) = x+y+z
    do i = 1, 4
       ! skip this center if encountered before
       if (any(c(:i-1)==c(i))) cycle
       ! increment to other occurences of center i
       do j = i+1, 4
          if (c(j)/=c(i)) cycle
          increment_mult = &
                increment_pair(x(i),y(i),z(i),x(j),y(j),z(j),f)
          if (increment_mult) exit
          call carry_pair(x(i),y(i),z(i),x(j),y(j),z(j),f)
       end do
       ! redistribute ti yi zi
       do j = j-1, i+1, -1
          if (c(j)/=c(i)) cycle
          call uncarry_pair(t(j),x(i),y(i),z(i),x(j),y(j),z(j),f)
       end do
       if (increment_mult) exit
    end do
  end function


  ! reset multiplicities x y z according to total 't'
  function reset_mult(c, t, x, y, z, f)
    integer, intent(in)    :: c(4), t(4)       !cent, total deriv orders
    integer, intent(inout) :: x(4), y(4), z(4) !ord of x- y- z-deriv
    integer, intent(out)   :: f                !multinomial prefactor
    logical reset_mult
    integer i, j, ti
    reset_mult = .true.
    f = 0
    ! first collect all in first occurence
    do i = 1, 4
       ! skip this center if encountered before, thus each
       if (any(c(:i-1)==c(i))) cycle !unique center once
       ! find other occurences of center c(i)
       ti = t(i)
       do j = i+1, 4
          if (c(j)/=c(i)) cycle
          ti = ti + t(j)
          call carry_pair(x(i),y(i),z(i),x(j),y(j),z(j),f)
       end do
       ! record whether there is a mismatch
       if (ti /= x(i)+y(i)+z(i)) reset_mult = .false.
    end do
    f = 1
    ! if x y z couldn't be distributed according to t, return false
    if (.not.reset_mult) return
    ! redistribute x y z according to t
    do i = 1, 4
       if (any(c(:i-1)==c(i))) cycle
       do j = 4, i+1, -1
          if (c(j)/=c(i)) cycle
          call uncarry_pair(t(j),x(i),y(i),z(i),x(j),y(j),z(j),f)
       end do
    end do
  end function


  function increment_pair(xi, yi, zi, xj, yj, zj, f)
    integer, intent(inout) :: xi, yi, zi, xj, yj, zj, f
    logical increment_pair
    ! yixj -> xiyj
    if (yi/=0 .and. xj/=0) then
       f = (f * xj) / (xi+1) !x-factor
       f = (f * yi) / (yj+1) !y-factor
       xi = xi+1
       xj = xj-1
       yi = yi-1
       yj = yj+1
       increment_pair = .true.
    ! xxziyyyj -> yyyixxzj
    else if (zi/=0 .and. xj+yj/=0) then
       ! first transfer as many as possible xiyj-> yixj
       do yj = yj, max(0, yj-xi-1)+1, -1
          if (xi/=0) f = (f * xi) / (xj+1) !x-factor
          f = (f * yj) / (yi+1) !y-factor
          xi = xi-1
          xj = xj+1
          yi = yi+1
       end do
       ! then transfer one zixj to xizj
       if (xi/=-1) f = (f * xj) / (xi+1) !x-factor
       f = (f * zi) / (zj+1) !z-factor
       xi = xi+1
       xj = xj-1
       zi = zi-1
       zj = zj+1
       increment_pair = .true.
    else !or could not increment, so tell caller to 'carry'
       increment_pair = .false.
    end if
  end function


  ! collect multiplicities on i while updating the prefactor f
  subroutine carry_pair(xi, yi, zi, xj, yj, zj, f)
    integer, intent(inout) :: xi, yi, zi, xj, yj, zj, f
    do xj = xj, 1, -1
       f = (f * xj) / (xi+1)
       xi = xi+1
    end do
    do yj = yj, 1, -1
       f = (f * yj) / (yi+1)
       yi = yi+1
    end do
    do zj = zj, 1, -1
       f = (f * zj) / (zi+1)
       zi = zi+1
    end do
  end subroutine


  ! collect multiplicities on i while updating the factor f
  subroutine uncarry_pair(tj, xi, yi, zi, xj, yj, zj, f)
    integer, intent(in)    :: tj
    integer, intent(inout) :: xi, yi, zi, xj, yj, zj, f
    do xj = 0, min(tj,xi)-1
       f = (f * xi) / (xj+1)
       xi = xi-1
    end do
    do yj = 0, min(tj-xj,yi)-1
       f = (f * yi) / (yj+1)
       yi = yi-1
    end do
    do zj = 0, min(tj-xj-yj,zi)-1
       f = (f * zi) / (zj+1)
       zi = zi-1
    end do
  end subroutine


#if defined(VAR_MPI)
  !> \brief broadcasts the context of contractions
  !> \author Bin Gao
  !> \date 2012-10-30
  subroutine eri_ctrs_mpi_bcast(ctrs, root, comm)
    type(ctr_arg), intent(inout) :: ctrs(:)
    integer, intent(in) :: root
    integer, intent(in) :: comm
#include "mpif.h"
    integer dim_ctrs    !dimension of \var(ctrs)
    logical do_expect   !if calculating the expectation values
    integer dim_expect  !dimension of expectation values
    integer rank_proc   !rank of processor
    integer ictr        !incremental recorder over \var(ctrs)
    integer ierr        !error information
    ! gets the rank of processor
    call MPI_Comm_rank(comm, rank_proc, ierr)
    ! allocates matrices on worker processes
    dim_ctrs = size(ctrs)
    if (rank_proc/=root) then
      allocate(wrk_dens(dim_ctrs), stat=ierr)
      if (ierr/=0) stop "eri_ctrs_mpi_bcast>> failed to allocate wrk_dens!"
      allocate(wrk_fock(dim_ctrs), stat=ierr)
      if (ierr/=0) stop "eri_ctrs_mpi_bcast>> failed to allocate wrk_fock!"
    end if
    do ictr = 1, dim_ctrs
      call MPI_Bcast(ctrs(ictr)%geo, 1, MPI_INTEGER, root, comm, ierr)
      call MPI_Bcast(ctrs(ictr)%comp, 1, MPI_INTEGER, root, comm, ierr)
      call MPI_Bcast(ctrs(ictr)%ncor, 1, MPI_INTEGER, root, comm, ierr)
      ! broadcasts matrices
      if (rank_proc/=root) ctrs(ictr)%dens => wrk_dens(ictr)
      call mat_mpi_bcast(ctrs(ictr)%dens, root, comm)
      if (rank_proc/=root) ctrs(ictr)%fock_or_dens => wrk_fock(ictr)
      call mat_mpi_bcast(ctrs(ictr)%fock_or_dens, root, comm)
      ! broadcasts the number of expectation values
      if (rank_proc==root) do_expect = associated(ctrs(ictr)%average)
      call MPI_Bcast(do_expect, 1, MPI_LOGICAL, root, comm, ierr)
      if (do_expect) then
        if (rank_proc==root) dim_expect = size(ctrs(ictr)%average)
        call MPI_Bcast(dim_expect, 1, MPI_INTEGER, root, comm, ierr)
        if (rank_proc/=root) then
          allocate(wrk_ave(dim_expect), stat=ierr)
          if (ierr/=0) stop "eri_ctrs_mpi_bcast>> failed to allocate wrk_ave!"
          ctrs(ictr)%average => wrk_ave
          ctrs(ictr)%average = 0.0
        end if
      else
        if (rank_proc/=root) then
!FIXME: do not touch elms
          ctrs(ictr)%fock_or_dens%elms = 0.0
          nullify(ctrs(ictr)%average)
        end if
      end if
    end do

    call MPI_Bcast(xfac, 1, MPI_DOUBLE_PRECISION, root, comm, ierr)

  end subroutine


  !> \brief reduces the context of contractions on all processes
  !> \author Bin Gao
  !> \date 2012-10-30
  subroutine eri_ctrs_mpi_reduce(ctrs, root, comm)
    type(ctr_arg), intent(inout) :: ctrs(:)
    integer, intent(in) :: root
    integer, intent(in) :: comm
#include "mpif.h"
    integer rank_proc  !rank of processor
    integer num_elms   !number of elements in the matrix
    integer ictr       !incremental recorder over \var(ctrs)
    integer ierr       !error information
    ! gets the rank of processor
    call MPI_Comm_rank(comm, rank_proc, ierr)
    do ictr = 1, size(ctrs)
      ! expectation values
      if (associated(ctrs(ictr)%average)) then
        if (rank_proc==root) then
          call MPI_Reduce(MPI_IN_PLACE, ctrs(ictr)%average,               &
                          size(ctrs(ictr)%average), MPI_DOUBLE_PRECISION, &
                          MPI_SUM, root, comm, ierr)
        else
          call MPI_Reduce(ctrs(ictr)%average, ctrs(ictr)%average,         &
                          size(ctrs(ictr)%average), MPI_DOUBLE_PRECISION, &
                          MPI_SUM, root, comm, ierr)
        end if
      ! Fock matrices
      else
!FIXME: move to matrix routines
        num_elms = ctrs(ictr)%fock_or_dens%nrow &
                 * ctrs(ictr)%fock_or_dens%ncol &
                 * ctrs(ictr)%fock_or_dens%algebra
        if (rank_proc==root) then
          call MPI_Reduce(MPI_IN_PLACE, ctrs(ictr)%fock_or_dens%elms, &
                          num_elms, MPI_DOUBLE_PRECISION, MPI_SUM, root, comm, ierr)
        else
          call MPI_Reduce(ctrs(ictr)%fock_or_dens%elms, &
                          ctrs(ictr)%fock_or_dens%elms, &
                          num_elms, MPI_DOUBLE_PRECISION,     &
                          MPI_SUM, root, comm, ierr)
        end if
      end if
    end do
  end subroutine
#endif

  !> \brief cleans the context of contractions
  !> \author Bin Gao
  !> \date 2012-10-30
  subroutine eri_ctrs_destroy(ctrs)
    type(ctr_arg), intent(inout) :: ctrs(:)
    integer ictr
    do ictr = 1, size(ctrs)
      ctrs(ictr)%geo = 0
      ctrs(ictr)%comp = 0
      ctrs(ictr)%ncor = 0
      call mat_remove(ctrs(ictr)%dens)
      nullify(ctrs(ictr)%dens)
      call mat_remove(ctrs(ictr)%fock_or_dens)
      nullify(ctrs(ictr)%fock_or_dens)
      if (associated(ctrs(ictr)%average)) nullify(ctrs(ictr)%average)
    end do
#if defined(VAR_MPI)
    deallocate(wrk_dens)
    deallocate(wrk_fock)
    if (allocated(wrk_ave)) deallocate(wrk_ave)
#endif
  end subroutine


end module
